<?php
/**
 *  [target of add-Tracklist form. Only teachers are not allowed, admin ?]
 */
require_once 'accessControlAllowOrigin.php';
require_once 'listModel.php';

$result = $_POST;                           // to do sanitize $_POST input

if(isset($_POST['name']))
{
    /* Split data and put each in new variable */
    $newName = $result['name'];
    $newDes = $result['description'];
    $private = $result['private'];
    $sender = $_SESSION['userId'];
    $video = $result['videos'];
    $videos = explode(",", $video);
                                            // If variables not empty
    if(isset($newName) && isset($newDes) && isset($sender) && isset($private))
    {
        if(isListnameUnique($sender, $newName)) // If user didn't used the name before
        {
            $result = addList($newName, $newDes, $sender, $private);
            for($i=1; $i < count($videos); $i++)
                $newRes = addtracks($result['tlid'], $videos[$i], $i);
        }
        else                                    // OBS!! for testing only
            $result = array('error'=>'List name not unique for this user');
    }
    else
        $result = array('error'=>'Not all input field registered');
}
else
    $result = array('error'=>'No POST[name]');

//echo json_encode ($newRes);
echo json_encode ($result);
?>
