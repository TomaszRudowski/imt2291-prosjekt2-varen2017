<?php
//header('Access-Control-Allow-Origin: http://localhost:8080');
//header('Access-Control-Allow-Credentials: true');

// Level one of API. Updates video description and name, as well as deleteSubtext


require_once("db.php");
require_once("videoModel.php");

$result = array();
  $vobj = new Video($db, $_POST['vid']);
if ($_POST['direction'] == "update") {

  $vobj->updateTrack($_POST['name'], $_POST['description']);

} else if ($_POST['direction'] == "delete") {
  $vobj->remove();
}

echo json_encode($result);

 ?>
