<?php
require_once 'db.php';

function addSubtext($videoId, $vttFile) {
  global $db;
  $sql = 'INSERT INTO subtext (name, textfile) VALUES (?, ?)';
  $stm = $db->prepare ($sql);
  $stm->execute (array ($videoId, $vttFile));
}

?>
