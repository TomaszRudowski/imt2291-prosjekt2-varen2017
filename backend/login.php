<?php
/**
 *  [This file handles login by sign-in form]
 */
require_once 'accessControlAllowOrigin.php';
require_once 'userModel.php';
session_start();

$result = array();

if (isset($_POST['email'])) {
 $postEmail = $_POST['email'];
 $row = getUserDataByEmail($postEmail);
 if ($row) {
	 if (password_verify ($_POST['password'], $row['password'])) {
		 // correct password, store user id in session variable and prepare data to be sent to frontend
     $_SESSION['userId'] = $row['id'];
		 $result['success'] = 'OK';
		 $result['id'] = $row['id'];
		 $result['email'] = $postEmail;
		 $result['surename'] = $row['surename'];
		 $result['givenname'] = $row['givenname'];
		 $result['clearance'] = $row['clearance'];
     if (isset($_POST['remember'])) {
       // clear previous persistant login, if exists
       removePersistantLogin($row['id']);
       //create persistant login in db and send to client to be used as value new cookie
       // cookie value looks like this: "userId@identifier@token"
       $result['persistantLogin'] = createPersistantLogin ($row['id'], $row['givenname']);
     }
	 } else {
		 // incorrect password
		 $result['error'] = 'incorrect password';
	 }
 } else {
	 // email not found in db
	 $result['error'] ='user not found in db';
 }
} else {
	// email not sendt
	$result['error'] = 'Not all input field registered';
}

echo json_encode($result);
?>
