<?php
  // Level one function of API. Returns set number of videos to frontend
  require_once("db.php");
  require_once("videoModel.php");
  $result = array();

  $videos = getRecentTracks(50, $db);
  $vid = array();
  if($videos) {
    foreach ($videos as $key => $value) {
      array_push($vid, $value->returnJsonOfObject());
    }
    echo json_encode($vid);
  } else {
    echo json_encode($result);
  }



 ?>
