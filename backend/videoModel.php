<?php

//  $_SESSION['uid'] = "896d5ac5-24ff-11e7-ac06-c85b7619cced";
//session_start();

  include("db.php");
  class Video {
    public $videoId;
    public $videoUrl;
    public $name;
    public $desc;
    public $imgUrl;
    public $ctime;
    public $subtext;
    private $db;

    public function __construct ($d, $vidid = NULL) {
      $this->db = $d;
      if($vidid) {
        $this->getTrack($vidid);
        $this->imgUrl = $this->getThumbnail();
      }
    }

    private function getTrack($videoId) {
      //var_dump($videoId);
      $sql = 'SELECT video, description, name, id, owner, addTime FROM track WHERE id=? LIMIT 1';
      try {
          $stm = $this->db->prepare($sql);
          $stm->execute(array($videoId));
          $row = $stm->fetch(PDO::FETCH_ASSOC);
          // Trenger en IFlokke for sjekk om man far tilbake noe
          $this->videoId = $row['id'];
          $this->videoUrl = $row['video'];
          $this->name = $row['name'];
          $this->desc = $row['description'];
          $this->ctime = $row['addTime'];
          $this->subtext = $this->getSubtracks();
      } catch (Exception $e) {}
    }

    private function getSubtracks(){
        $sql = 'SELECT textfile, subtext.name FROM subtext INNER JOIN texttotrack ON textid = subtext.id INNER JOIN track ON trackid = track.id WHERE track.id = ?';
        try {
            $stm = $this->db->prepare($sql);
            $stm->execute(array($this->videoId));
            $row = $stm->fetch();
            $subtext = $row['textfile'];

            return $subtext;
        } catch (Exception $e) {}
    }

    public function addSubtext($tmp, $name) {
      try {
        $destination = dirname($this->videoUrl)."/".$name;
        move_uploaded_file($tmp, $new_path);
      } catch (Excetiopn $e) {}

      if($this->videoId) {
        $sql = 'INSERT INTO subtext (name, textfile) VALUES (?, ?);';
        try {
            $smt = $this->db->prepare($sql);
            $smt->execute(array($tmp, $destination));

            if($smt->rowCount() == 0) {
                die("kunne ikke legge inn tekst");
                return 0;
            }
            $textId = $this->db->lastInsertId();

            $sql = "INSERT INTO texttotrack (trackid, textid) VALUES (?, ?)";
            $smt = $this->db->prepare($sql);
            $smt->execute(array($this->videoId, $textId));

            if($this->subtext) {
              $this->deleteSubtext($this->subtext);
            }
            $this->subtext = $this->getSubtracks();
            return true;
        } catch (Exception $e) {}

    } else {
      $result['danger']['Idiot programmer'] = "a developer is drunk...";
      return false;

      // Det skal ikke være mulig å legge til en tekst på en ukjent videoId

    }
  }

  public function addSubtextLocal($name) {

    if($this->videoId) {
      $sql = 'INSERT INTO subtext (name, textfile) VALUES (?, ?);';
      try {
          $smt = $this->db->prepare($sql);
          $smt->execute(array($name, $name));

          if($smt->rowCount() == 0) {
              die("kunne ikke legge inn tekst");
              return 0;
          }
          $textId = $this->db->lastInsertId();

          $sql = "INSERT INTO texttotrack (trackid, textid) VALUES (?, ?)";
          $smt = $this->db->prepare($sql);
          $smt->execute(array($this->videoId, $textId));

          if($this->subtext) {
            $this->deleteSubtext($this->subtext);
          }
          $this->subtext = $this->getSubtracks();
      } catch (Exception $e) {}
  } else {
    // Det skal ikke være mulig å legge til en tekst på en ukjent videoId
    echo "<a href='http://php.net/manual/en/intro-whatis.php'>Edjucate yourself</a>";
  }
}

  public function remove () {
      // Track vil inneholde subtrackinfo
      if($this->videoId) {
        $videoInf = $this->getTrack($this->trackId);
        $sql = 'DELETE FROM track WHERE track.id = ?';
        try {
            $stm = $this->db->prepare($sql);
            $stm->execute(array($this->trackId));
            if($stm->rowCount() == 0) {
                $result['warning']['Database error'] = "could not remove track form database";
            }
            $stm->debugDumpParams();
            $path = explode('/', $videoInf->retPath());
            $directory = "/".$path[0]."/".$path[1]; // Mappen filene ligger i

            unlink($videoInf->retPath());
            foreach($videoInf->retSubs() as $sub => $key)
                unlink($key);
        }catch (Exception $e) {}
    } else {
      $result['warning']['Incompetent programmer'] = "The developer could not code the page correctly";
    }
  }

  public function deleteThumbnail($path) {
    $sql = "DELETE FROM thumbnail WHERE thumbnail = ?";
    $stm = $this->db->prepare($sql);
    $stm->execute(array($path));

    unlink($path);
  }

  public function add ($trackpath, $description, $name) {
		try {
      $sql = 'INSERT INTO track (id, video, description, name, owner) VALUES (UUID(), ?, ?, ?, ?)';
  		$stm = $this->db->prepare($sql);
  		$stm->execute ( array($trackpath, $description, $name, $_SESSION['userId']));
  		if ($stm->rowCount()==0) {
        return false;
  		} else {
        $this->getTrackByPath($trackpath);
        return true;
      }
  } catch (Exception $e) {}
  }

  private function getTrackByPath ($path) {
    try {
      $sql = 'SELECT id, description, name, addtime FROM track WHERE video=?';
      $stm = $this->db->prepare($sql);
      $stm->execute(array($path));
      $row = $stm->fetch(PDO::FETCH_ASSOC);

      if ($row) {
        //die("kunne ikke hente informasjon");
      } else {
        $this->videoId = $row['id'];
        $this->name = $row['name'];
        $this->desc = $row['description'];
        $this->ctime = $row['addTime'];
      }

  } catch (Exception $e) {}
}

private function deleteSubtext($path = NULL) {
  try {
    if($path)
      $target = $path;
    else
      $target = $this->subtext;
    if($target) {
    $sql = "DELETE FROM subtext WHERE textfile = ?";
    $stm = $this->db->prepare($sql);
    $stm->execute(array($target));
    unlink($subtext); // Removes file from disk
    }

  } catch (Exception $e) {}

}

  public function getFolder() {
    return dirname($this->videoUrl);
  }

private function getThumbnail() {
  try {
    $sql = "SELECT thumbnail FROM thumbnail WHERE vid = ?";
    $stm = $this->db->prepare($sql);
    $stm->execute(array($this->videoId));
    $row = $stm->fetch(PDO::FETCH_ASSOC);

    if ($row) {
      return $row['thumbnail'];
    } else {
      return NULL;
    }
  } catch (Exception $e) {}
}

public function returnJsonOfObject() {
  $arr = array();
  $arr['path'] = $this->videoUrl;
  $arr['subtext'] = $this->subtext;
  $arr['name'] = $this->name;
  $arr['desc'] = $this->desc;
  $arr['id'] = $this->videoId;
  $arr['thumbnail'] = $this->imgUrl;
  $arr['time'] = $this->ctime;
  $arr['root'] = $this->getFolder();
  return $arr;
}

public function updateTrack ($name, $description) {
   $sql = 'UPDATE track SET name=?, description=? WHERE id=? LIMIT 1';
   try {
       $stm = $this->db->prepare($sql);
       $stm->execute(array($name, $description, $this->videoId));
       if($stm->rowCount() == 0) {
         $result['warning']["Line: ".__LINE__." File: ".__FILE__] = "No tracks were updated";
       } else {
         $result['succsess']['Update complete'] = "update complete";
       }
   } catch (Exception $e) {}
 }

 public function addThumbnail($tmpLocation) {
   $existingThumb = $this->getThumbnail();
   // Eventuelt fjern fra databasen om det går galt
   $thumbPath = dirname($this->videoUrl). "/" . basename($tmpLocation);
   $sql = "INSERT INTO thumbnail (thumbnail, vid) VALUES (?, ?)";
   $stm = $this->db->prepare($sql);
   $stm->execute(array($thumbPath, $this->videoId));

   if($stm->rowCount() != 0) {
     //var_dump($this->returnJsonOfObject() ,$this->videoUrl, $tmpLocation, $thumbPath);
     if(move_uploaded_file($tmpLocation, $thumbPath)) {
       if($existingThumb) {
         $this->deleteThumbnail($existingThumb);
       }
      return true;
    } else {
      $this->deleteThumbnail($thumbPath);
    }
   } else {
     return false;
   }

 }
};

function getTracksByUser($userID, $db) {
   $sql = 'SELECT id FROM track WHERE owner=? ORDER BY addtime DESC';
   try {
       $stm = $db->prepare($sql);
       $stm->execute(array($userID));
       $row = $stm->fetchAll(PDO::FETCH_ASSOC);
       $videos = array();
       if($stm->rowCount() == 0) {
           $result['warning']['No entries'] = "found no entries for this user";
           return null;
       }

       foreach($row as $key) {
           array_push($videos, new Video($db, $key['id']));
       }
       return $videos;
   } catch (Exception $e) {}
 }

 function getTracksBySearchWord($searchWord, $db) {
   // problems with wildcards and LIKE, $searchWord is sanitized after input.
   $sql = 'SELECT id FROM track WHERE name LIKE "%'.$searchWord.'%" OR description LIKE "%'.$searchWord.'%" ORDER BY addTime DESC';
   try {
       $stm = $db->prepare($sql);
       $stm->execute(array());
       $row = $stm->fetchAll(PDO::FETCH_ASSOC);
       $videos = array();
       if($stm->rowCount() == 0) {
           $result['warning']['No entries'] = "found no entries for this user";
           return null;
       }

       foreach($row as $key) {
           array_push($videos, new Video($db, $key['id']));
       }
       return $videos;
   } catch (Exception $e) {}
 }


function getRecentTracks($number, $db) {
  $sql = 'SELECT id FROM track ORDER BY addtime DESC LIMIT ?';
  try {
      $stm = $db->prepare($sql);
      $stm->bindValue(1, $number, PDO::PARAM_INT);
      $stm->execute();
      $row = $stm->fetchAll(PDO::FETCH_ASSOC);
      $videos = array();
      if($stm->rowCount() == 0) {
          $result['warning']['No entries'] = "found no entries for this user";
          return null;
      }

      foreach($row as $key) {
          array_push($videos, new Video($db, $key['id']));
      }
      return $videos;
  } catch (Exception $e) {}
}
//  $vTest->add("SomePathToVideo", "SomeLongDescription", "SomeStupidName");
