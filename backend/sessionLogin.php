<?php
/**
 *  [This file handles login using session variable set as cookie PHPSESSID]
 */
require_once 'accessControlAllowOrigin.php';
require_once 'userModel.php';
session_start();

$result = array();

if (isset($_SESSION['userId'])) {

  $row = getUserDataById($_SESSION['userId']);
  if ($row) {
    $result['success'] = 'Session reestablished';
    $result['id'] = $row['id'];
    $result['email'] = $row['email'];
    $result['surename'] = $row['surename'];
    $result['givenname'] = $row['givenname'];
    $result['clearance'] = $row['clearance'];
  } else {
  // email not found in db
    $result['error'] ='user not found in db';
  }
  //$result['id'] = $_SESSION['PHPSESSID'];
  //$result['cookie'] = $_POST['PHPSESSID'];
} else {
	// email not sendt
	$result['error'] = 'session not established';
}

echo json_encode($result);
?>
