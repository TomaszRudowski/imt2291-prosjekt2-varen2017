set global innodb_large_prefix=on;

CREATE TABLE user (id VARCHAR(100),
  email VARCHAR(100),
  PRIMARY KEY(id, email),
  password VARCHAR(100) NOT NULL,
  surename VARCHAR(80), givenname VARCHAR(100),
  clearance SMALLINT);


CREATE TABLE persistedlogin (identifier VARCHAR(100),
  token VARCHAR(100),
  uid VARCHAR(100),
  CONSTRAINT fk_userpersist FOREIGN KEY (uid)
  REFERENCES user(id) ON DELETE CASCADE);

CREATE TABLE tracklist (tlid BIGINT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  description TEXT,
  addtime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  owner VARCHAR(100) NOT NULL, CONSTRAINT fk_tracklistowner FOREIGN KEY (owner)
  REFERENCES user(id),
  private BOOLEAN DEFAULT 0);

CREATE TABLE track (id VARCHAR(100) PRIMARY KEY,
  video VARCHAR(100) NOT NULL,
  description TEXT,
  name VARCHAR(100) NOT NULL,
  addtime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  owner VARCHAR(100) NOT NULL, CONSTRAINT fk_trackowner FOREIGN KEY (owner)
  REFERENCES user(id));

CREATE TABLE trackposition (position INT,
  tracklistid BIGINT,
  trackid VARCHAR(100),
  PRIMARY KEY(position, tracklistid, trackid),
  CONSTRAINT fk_trackposition FOREIGN KEY (trackid)
  REFERENCES track(id) ON DELETE CASCADE,
  CONSTRAINT fk_tracklistposition FOREIGN KEY (tracklistid)
  REFERENCES tracklist(tlid) ON DELETE CASCADE);

CREATE TABLE subtext (id BIGINT AUTO_INCREMENT,
  name VARCHAR (100) NOT NULL,
  textfile LONGTEXT NOT NULL,
  PRIMARY KEY (id));

CREATE TABLE texttotrack (trackid VARCHAR(100) NOT NULL,
  textid BIGINT NOT NULL,
  PRIMARY KEY(trackid, textid),
  CONSTRAINT fk_texttotrack FOREIGN KEY (trackid)
  REFERENCES track(id) ON DELETE CASCADE,
  CONSTRAINT fk_subtextid FOREIGN KEY (textid)
  REFERENCES subtext(id) ON DELETE CASCADE);


CREATE TABLE log (tid TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, uid VARCHAR(100), vid VARCHAR(255),
  PRIMARY KEY(tid, uid),
  CONSTRAINT fk_userlog FOREIGN KEY (uid)
  REFERENCES user(id) ON DELETE CASCADE,
  CONSTRAINT fk_videolog FOREIGN KEY (vid)
  REFERENCES track(id) ON DELETE CASCADE);

CREATE TABLE thumbnail (id INT AUTO_INCREMENT PRIMARY KEY, thumbnail VARCHAR(255),
  vid VARCHAR(255),
  CONSTRAINT fk_vidthumbnail FOREIGN KEY (vid)
  REFERENCES track(id) ON DELETE CASCADE);

CREATE TABLE imagefile (id BIGINT AUTO_INCREMENT,
  name VARCHAR (100) NOT NULL,
  textfile LONGTEXT NOT NULL,
  PRIMARY KEY (id));

CREATE TABLE imagetotrack (trackid VARCHAR(100) NOT NULL,
  textid BIGINT NOT NULL,
  PRIMARY KEY(trackid, textid),
  CONSTRAINT fk_imgfiletotrack FOREIGN KEY (trackid)
  REFERENCES track(id) ON DELETE CASCADE,
  CONSTRAINT fk_imgfileid FOREIGN KEY (textid)
  REFERENCES imagefile(id) ON DELETE CASCADE);
