<?php
/**
 *  [target of add-Tracklist form. Only teachers are not allowed, admin ?]
 */
require_once 'accessControlAllowOrigin.php';
require_once 'listModel.php';

$result = $_POST;                           // to do sanitize $_POST input

if(isset($_POST['listId']))
{
    /* Split data and put each in new variable */
    $newId = $result['listId'];
    $sender = $_SESSION['userId'];
    
    if(isset($newId) && isset($sender))
        $result = getListById($newId, $sender);
    else
        $result = array('error'=>'Some of data is empty');
}
else
    $result = array('error'=>'No POST[name]');

//echo json_encode ($newRes);
echo json_encode ($result);
?>
