<?php
/**
 *  [This file handles login using cookie persistantLogin, after checkbox "remember me" is checked during login]
 */
require_once 'accessControlAllowOrigin.php';
require_once 'userModel.php';
session_start();

$result = array();

if (isset($_COOKIE['persistantLogin'])) {
  // split cookie from value "userId@identifier@token"
  $persistantData = explode("@", $_COOKIE['persistantLogin']);

  $userId = $persistantData[0];
  $identifier = $persistantData[1];
  $token = $persistantData[2];
  // verify cookie
  $foundToken = findPersistedLogin($userId, $identifier);

  if ($foundToken['token']==$token) {
        // get userdata from db
    $row = getUserDataById($userId);
    if ($row) {
      $result['success'] = 'Session reestablished using persistant login';
      $result['id'] = $row['id'];
      $result['email'] = $row['email'];
      $result['surename'] = $row['surename'];
      $result['givenname'] = $row['givenname'];
      $result['clearance'] = $row['clearance'];
      // renew persistant login:
      // 1. delete old persistant login
      removePersistantLogin($userId);
      // 2. add to db and send new cookie value
      $result['persistantLogin'] = createPersistantLogin ($row['id'], $row['givenname']);
    } else {
      $result['error'] ='user not found in db';
    }
  }  else {
  	$result['error'] = 'Persistant login token not verified';
  }
} else {
  $result['error'] ='Cookie not received';
}
// return it
echo json_encode($result);



?>
