<?php
/**
 *  [target of sign-up user form. Student-user(3) can be self-registered, but Teacher-user(2) can be added by Admin(1) only]
 *  [Important: Admin-user(1) can be added only by manual altering data in db, the only way to get clearance(1) user]
 */
require_once 'accessControlAllowOrigin.php';
require_once 'userModel.php';

$result = $_POST;
// to do sanitize $_POST input
if (isset($_POST['email'])) {
  $newEmail = $result['email'];
  $newPwdHash = password_hash($result['password'], PASSWORD_DEFAULT);
  $newGivenname = $result['givenname'];
  $newSurename = $result['surename'];
  $newClearance = $result['clearance'];
  $sender = $result['sender'];
  if($newEmail && $newPwdHash && $newGivenname && $newSurename && $newClearance) {
    if (getClearance($sender) == 1) {    // Admin tries to add user
     switch ($newClearance) {
       case "2": ;   // add Teacher
       case "3":     // add Student
          if (isEmailUnique($newEmail)) {
            $result = addUser($newEmail, $newPwdHash, $newSurename, $newGivenname, $newClearance);
          } else {
           $result = array('error'=>'Email not unique'); // OBS!! for testing only
          }
          break;
       default:       // error !! corrupted $_POST data
         $result = array('error'=>'Admin can add only Student or Teacher user');
     }
    } else {                             // non-Admin tries to add user
     if ($newClearance == 3) {          // add Student
          if (isEmailUnique($newEmail)) {
            $result = addUser($newEmail, $newPwdHash, $newSurename, $newGivenname, $newClearance);
          } else {
           $result = array('error'=>'Email not unique'); // OBS!! for testing only
          }
     } else {                           // errror !! restricted to Admin
       $result = array('error'=>'Non-Admin can add only Student user');
     }
    }
  } else {
    $result = array('error'=>'Not all input field registered');
  }
} else {
	$result = array('error'=>'No POST[email]');
}
echo json_encode ($result);

?>
