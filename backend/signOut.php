<?php
require_once 'accessControlAllowOrigin.php';
require_once 'userModel.php';
session_start();

$result = array();

if (isset($_SESSION['userId'])) {
  $userId = $_SESSION['userId'];
  //$result['user']=$userId; //if need some sort of history/log previous user
  removePersistantLogin($userId); // clear db
}


// -- this block from http://php.net/manual/en/function.session-destroy.php
// Unset all of the session variables.
$_SESSION = array();

// If it's desired to kill the session, also delete the session cookie.
// Note: This will destroy the session, and not just the session data!
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}
// Finally, destroy the session.
session_destroy();
// -- end of block from http://php.net/manual/en/function.session-destroy.php

$result['logout'] = "true";
$result['success'] = "Session data cleared";
echo json_encode($result);
?>
