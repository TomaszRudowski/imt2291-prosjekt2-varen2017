<?php

require_once 'db.php';
/** Search through tracklist table to find matching $searchWord in tracklist.name or tracklist.description
*
* @param string $searchWord
*
* @return array, querry results as associative array
*/
	function searchTracklist ($searchWord) {
    global $db;
		$sql = 'SELECT tlid, name, description, addTime  FROM tracklist
			WHERE name LIKE "%'.$searchWord.'%" OR description LIKE "%'.$searchWord.'%"
			ORDER BY addTime DESC';
		try {$stm = $db->prepare($sql);
            $stm->execute(array ());
            $res = $stm->fetchAll(PDO::FETCH_ASSOC);
            return $res;
        } catch (Exception $e) {}
	}

/** Search through track table to find matching $searchWord in track.name or track.description
*
* @param string $searchWord
*
* @return array, querry results as associative array
*/
	function searchVideos ($searchWord) {
    global $db;
		$sql = 'SELECT id, video, description, name, addTime FROM track
			WHERE name LIKE "%'.$searchWord.'%" OR description LIKE "%'.$searchWord.'%"
			ORDER BY addTime DESC';
		try {
            $stm = $db->prepare($sql);
            $stm->execute(array ());
            $res = $stm->fetchAll(PDO::FETCH_ASSOC);
            return $res;
        } catch (Exception $e) {}
	}

 ?>
