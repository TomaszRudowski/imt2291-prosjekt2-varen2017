<?php
/**
 *  [target of showList. Only teachers are not allowed]
 */
require_once 'accessControlAllowOrigin.php';
require_once 'listModel.php';

$result = getAllMyList($_SESSION['userId']);

echo json_encode ($result);
?>
