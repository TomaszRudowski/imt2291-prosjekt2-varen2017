<?php

$result = array();
$ok = 1;
require_once("videoModel.php");
error_reporting(E_ERROR);
require_once("db.php");
//session_start();
require_once("accessControlAllowOrigin.php");

$result['info']['Video ID'] = $_POST['vid'];

$imgFormats = array("jpg", "jpeg", "png", "gif", "bmp");

if(isset($_POST['vid']) && isset($_FILES['file'])) {
  $correctFormat = 0;
  foreach ($imgFormats as $key => $value) {
    if(("image/".$value) == $_FILES['file']['type']){
      $correctFormat = 1;
      break;
    }
  }
  if(!$correctFormat) {
    $result['danger']['file format'] = "Uploaded file has wrong file type";
    $ok = 0;
  }
} else {
  $result['danger']['Error'] = "did not recieve required data";
  $ok = 0;
}

if($_FILES['file']['size'] > 50000000) {
  $ok = 0;
  $result['danger']['filesize'] = "File is larger than 50MB. It is an image god dammit";
}

if ($ok) {
  $model = new Video($db, $_POST['vid']);
  if($model->addThumbnail($_FILES['file']['tmp_name'])) {
    $result['success']['upload'] = "File uploaded";
  } else {
    $result['danger']['Error'] = "could not upload file";
  }
}

echo json_encode($result);
 ?>
