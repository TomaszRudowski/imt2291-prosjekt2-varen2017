<?php


require_once ("db.php");
require_once ("userModel.php");

session_start();

if(isset($_SESSION['userId']) && isset($_POST['vid'])) {
  $res['info']['Log information'] = $_SESSION['userId'].$_POST['vid'];
  // Do log stuff
  registerLog($_SESSION['userId'], $_POST['vid']);
} else if ($_SESSION['userId']) {
  $res = getLogByOwner($_SESSION['userId']);
} else {
  $res['info']['Not logged in'] = "I dont log guests. What am I? FACEBOOK?!?!";
}
echo json_encode($res);


 ?>
