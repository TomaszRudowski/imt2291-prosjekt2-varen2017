<?php
require_once 'accessControlAllowOrigin.php';
require_once 'userModel.php';
require_once 'subsModel.php';
require_once 'searchModel.php';
require_once "videoModel.php";

//session_start();

$result = array();
$searchWord;
// setup search word
if ( isset($_POST['searchWord'])) {
  $searchWord = $_POST['searchWord'];
} else {
  $searchWord = "";
}
/*
// setup current user (in purpose of edit/view possibilities)
if (isset($_SESSION['userId'])) {
  $user = $_SESSION['userId'];
} else {
	$user = "";
}*/
$searchWordSanitized = filter_var($searchWord, FILTER_SANITIZE_STRING);
$videos = getTracksBySearchWord($searchWordSanitized, $db);
if($videos) {
  foreach ($videos as $key => $value) {
    array_push($result, $value->returnJsonOfObject());
  }
} 
  echo json_encode($result);

//$result ['resList'] = searchTracklist ($searchWord);
//$result = searchVideos ($searchWord);

 ?>
