<?php
/**
 *  File with all functions releted to user proccessing data
 */
require_once 'db.php';

/**
 *  [getUserIdByEmail gets user data from db]
 *  @method getUserIdByEmail
 *  @param  [string]           $email [description]
 *  @return [array]                  [description]
 */
function getUserIdByEmail($email) {
  global $db;
  $sql = 'SELECT id FROM user WHERE email=?';
  try {
  $stm = $db->prepare ($sql);
  $stm->execute (array ($email));
  $res = $stm->fetch(PDO::FETCH_ASSOC);
  return $res['id'];
  } catch (Exception $e) {}
}

/**
 *  [getUserDataByEmail gets user data from db]
 *  @method getUserDataByEmail
 *  @param  [string]             $email [description]
 *  @return [array]                    [description]
 */
function getUserDataByEmail($email) {
  global $db;
  $sql = 'SELECT id, email, password, surename, givenname, clearance FROM user WHERE email=?';
  try {
  $stm = $db->prepare ($sql);
  $stm->execute (array ($email));
  return $stm->fetch(PDO::FETCH_ASSOC);
  } catch (Exception $e) {}
}

/**
 *  [getUserDataById gets user data from db]
 *  @method getUserDataById
 *  @param  [string]          $id [description]
 *  @return [array]              [description]
 */
function getUserDataById($id) {
  global $db;
  $sql = 'SELECT id, email, password, surename, givenname, clearance FROM user WHERE id=?';
  try {
  $stm = $db->prepare ($sql);
  $stm->execute (array ($id));
  return $stm->fetch(PDO::FETCH_ASSOC);
  } catch (Exception $e) {}
}

/**
 *  [isEmailUnique tests if email is alleready registered in db]
 *  @method isEmailUnique
 *  @param  [string]        $email [description]
 *  @return boolean              [description]
 */
function isEmailUnique($email) {
  global $db;
  $sql = 'SELECT email FROM user WHERE email=?';
  try {
  $stm = $db->prepare ($sql);
  $stm->execute (array ($email));
  if ($stm->rowCount()==0) {
    return true;
  } else {
    return false;
  }
  } catch (Exception $e) {}
}

/**
 *  [addUser adds row to db, if possible]
 *  @method addUser
 *  @param  [string]  $newEmail     [description]
 *  @param  [string]  $newPwdHash   [description]
 *  @param  [string]  $newSurename  [description]
 *  @param  [string]  $newGivenname [description]
 *  @param  [string]  $newClearance [description]
 *  @return [array]                 [description]
 */
function addUser($newEmail, $newPwdHash, $newSurename, $newGivenname, $newClearance) {
  global $db;
  $sql = 'INSERT INTO user (id, email, password, surename, givenname, clearance) VALUES (UUID(), ?, ?, ?, ?, ?)';
  try {
  $stm = $db->prepare ($sql);
  $stm->execute (array ($newEmail, $newPwdHash, $newSurename, $newGivenname, $newClearance));
  if ($stm->rowCount()==0) {
   return array ('error'=>'email unique but not added of other reason');
 } else {
   $res = array();
   $res['id'] = getUserIdByEmail($newEmail);
   $res['email'] = $newEmail;
   $res['givenname'] = $newGivenname;
   $res['surename'] = $newSurename;
   $res['clearance'] = $newClearance;
   $res['success'] = 'OK';
   return $res;
 }
 } catch (Exception $e) {}
}

/**
 *  [getClearance is used to check if an action could be performed according to clearance level]
 *  @method getClearance
 *  @param  [string]       $userId [description]
 *  @return [int]               [1-admin, 2-teacher, 3-student, 4-guest]
 */
function getClearance($userId) {
  global $db;
  $sql = 'SELECT clearance FROM user WHERE id=?';
  try {
    $stm = $db->prepare ($sql);
    $stm->execute (array ($userId));
    $res = $stm->fetch(PDO::FETCH_ASSOC);
    if (!isset($res['clearance'])) {  // $userId not found in db
      $res['clearance'] = 4;      // access as not registered user
    }
    return $res['clearance'];
  } catch (Exception $e) {}
}

/**
 *  [createPersistantLogin when checkbox 'remember me' was used]
 *  @method createPersistantLogin
 *  @param  [string]                $userId [description]
 *  @param  [string]                $userName [description]
 *  @return [string]                        [cookie value]
 */
function createPersistantLogin ($userId, $userName) {
  global $db;
  $identifier =  md5($userName.(time ()));
  $token = md5($userId.(time()));
  $cookieValue = $userId."@".$identifier."@".$token;
  // Insert in db
  $sql = "INSERT INTO persistedlogin (uid, identifier, token) VALUES (?, ?, ?)";
  try {
    $stm = $db->prepare($sql);
    $stm->execute (array ($userId, $identifier, $token));

    return $cookieValue;
  } catch (Exception $e) {}
}

/**
 *  [findPersistedLogin finds token in db and return to be compared with received cookie]
 *  @method findPersistedLogin
 *  @param  [string]             $uid        [description]
 *  @param  [string]             $identifier [description]
 *  @return [array]                         [description]
 */
function findPersistedLogin($uid, $identifier) {
  global $db;
  $sql = "SELECT token FROM persistedlogin WHERE uid=? AND identifier=?";
  try {
    $stm = $db->prepare ($sql);
    $stm->execute (array ($uid, $identifier));
    return $stm->fetch(PDO::FETCH_ASSOC);
  } catch (Exception $e) {}
}

/**
 *  [removePersistantLogin removes persistant login info from db]
 *  @method removePersistantLogin
 *  @param  [string]                $uid [description]
 *  @return [type]                     [description]
 */
function removePersistantLogin($uid) {
  global $db;
  $sql = "DELETE FROM persistedlogin WHERE uid=?";
  try {
    $stm = $db->prepare($sql);
    $stm->execute (array ($uid));
  } catch (Exception $e) {}
}

function deleteUser($uid) {
  global $db;
  $sql = "DELETE FROM user WHERE id = ?";
  try {
    $stm = $db->prepare($sql);
    $stm->execute(array($uid));
  } catch (Exception $e) {}
}


function getAllUsersExceptAdmin() {
  global $db;
  $array = array();
  $sql = "SELECT id, email, surename, givenname FROM user WHERE clearance != 1";

  try {
    $stm = $db->prepare($sql);
    $stm->execute();
    $row = $stm->fetchAll(PDO::FETCH_ASSOC);

    return $row;
  } catch (Exception $e) {}
}

/**
 *  [Get]
 *  @method getWatchUserByTid
 *  @param  [string]                $vid [Video ID]
 *  @return [Array]                      [Data]
 */
function getWatchUserByTid($vid)
{
    global $db;
    $sql = "SELECT tid, user.surename, user.givenname, tid FROM log INNER JOIN user ON user.id = log.uid WHERE vid = ?";

    $stm = $db->prepare($sql);
    $stm->execute (array ($vid));
    $res = $stm->fetchAll(PDO::FETCH_ASSOC);

    return $res;
}

/**
 *  [Get]
 *  @method registerLog
 *  @param  [String]                 [uId = userID]
 *  @param  [String]                 [vId = VideoID]
 *  @return [Array]                         [Success message or error]
 */
function registerLog($uId, $vId)
{
    global $db;
    $sql = "INSERT INTO log (uid, vid) VALUES (?,?)";

    $stm = $db->prepare($sql);
    $stm->execute (array ($uId, $vId));
    $res = $stm->fetchAll(PDO::FETCH_ASSOC);

    if ($stm->rowCount()==0) {               // Do a check if done
        $res['warning']['log incomplete'] = 'play not logged';
        return array ('error'=>'can not add it, for some reason');
      }

    else                                    // ok good, give me db result
    {
        $res = array();
        $res['success']['log complete'] = 'OK';
        return $res;
    }
}
// Gets a log of which students have watched which videoUrl
function getLogByOwner($oid) {
  global $db;
  $sql = "SELECT tid, name, surename, givenname, email FROM log INNER JOIN track on track.id = log.vid INNER JOIN user ON log.uid = user.id WHERE clearance = 3 AND OWNER = ? ORDER BY tid DESC";
  try {
    $stm = $db->prepare($sql);
    $stm->execute(array($oid));

    $result = $stm->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  } catch (Exception $e) {
    return NULL;
  }
}

?>














