<?php
// Level one of backend API. Returns array of video-objets
session_start();

require_once "videoModel.php";
require_once "db.php";

  $videos = getTracksByUser($_SESSION['userId'], $db);
  $result = array();
  foreach ($videos as $key => $value) {
    array_push($result, $value->returnJsonOfObject());
  }
  echo json_encode($result);



 ?>
