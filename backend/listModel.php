<?php
/**
 *  File with all functions releted to Tracklist proccessing data
 */
require_once 'db.php';
session_start();

/**
 *  [isListnameUnique tests if the new list name is new for this user]
 *  @method isListnameUnique
 *  @param  [string]                        [$id = tracklistID]
 *  @param  [string]                        [name = tracklist name]
 *  @return boolean                         [True if done, false if not]
 */
function isListnameUnique($id, $name)
{
    global $db;
    $sql = 'SELECT name FROM tracklist WHERE owner=? AND name=?';
    $stm = $db->prepare ($sql);
    $stm->execute (array ($id, $name));
    if ($stm->rowCount()==0)
        return true;
    else
        return false;
}

/**
 *  [addList adds row to db, if possible]
 *  @method addUser
 *  @param  [string]    	                [newName = tracklist name]
 *  @param  [string]                        [newDes = tracklist Description]
 *  @param  [string]                        [Sender = User ID]
 *  @param  [int]                           [Sender = private]
 *  @return [Array]                         [Getdata back to let user check with success]
 */
function addList($newName, $newDes, $sender, $private)
{
    global $db;
    $sql = 'INSERT INTO tracklist (name, description, owner, private) VALUES (?, ?, ?, ?)';
    $stm = $db->prepare ($sql);
    $stm->execute (array ($newName, $newDes, $sender, $private));

    if ($stm->rowCount()==0)                // Do a check if done
        return array ('error'=>'List name unique but not added of other reason');
    else                                    // ok good, give me db result
    {
        $res = array();
        $res['tlid'] = getListIdByName($newName, $sender);
        $res['name'] = $newName;
        $res['description'] = $newDes;
        $res['addtime'] = getAddtimeForList(getListIdByName($newName, $sender));
        $res['owner'] = $sender;
        $res['private'] = $private;
        $res['success'] = 'OK';
        return $res;
    }
}

/**
 *  [getListIdByName gets List data from db]
 *  @method getListIdByName
 *  @param  [string]    	                [newName = tracklist name]
 *  @param  [string]                        [Sender = User ID]
 *  @return [int]                           [tld = Tracklist ID]
 */
function getListIdByName($newName, $sender)
{
    global $db;
    $sql = 'SELECT tlid FROM tracklist WHERE owner=? AND name=?';
    $stm = $db->prepare ($sql);
    $stm->execute (array ($sender, $newName));
    $res = $stm->fetch(PDO::FETCH_ASSOC);
    return $res['tlid'];
}

/**
 *  [getAddtimeForList gets Addtime for a List from db]
 *  @method getAddtimeForList
 *  @param  [string]                        [$id = tracklistID]
 *  @return [int]                           [Tracklist id if found, 0 if not]
 */
function getAddtimeForList($id)
{
    global $db;
    $sql = 'SELECT addtime FROM tracklist WHERE tlid=?';
    $stm = $db->prepare ($sql);
    $stm->execute (array ($id));
    $res = $stm->fetch(PDO::FETCH_ASSOC);

    if (!isset($res['clearance']))      //If ID has not been found
        $res['tlid'] = 0;               // Put as 0 to see the error
    return $res['tlid'];                // Return tracklist Id og 0
}

/**
 *  [getAllMyList gets All tracklist that user have from db]
 *  @method getAllMyList
 *  @param  [string]                        [uId = User ID]
 *  @return [array]                         [List of users tracklists object]
 */
function getAllMyList($uId)
{
    global $db;
    $userTracklists = array();
    $sql = 'SELECT tlid, name, description, addtime, private FROM tracklist WHERE owner=?';
    
    $stm = $db->prepare ($sql);
    $stm->execute (array ($uId));
    $userTracklists = $stm->fetchAll(PDO::FETCH_ASSOC);
    
    return $userTracklists;
}

/**

 *  [addTracks to list row to db, if possible]
 *  @method addUser
 *  @param  [string]                        [$lid = tracklistID]
 *  @param  [string]                        [$tid = trackid]
 *  @param  [string]                        [$pos = position]
 *  @return [Array]                         [Success message or error]
 */
function addtracks($lid, $tid, $pos)
{
    global $db;
    $sql = 'INSERT INTO trackposition (position, tracklistid, trackid) VALUES (?, ?, ?)';
    $stm = $db->prepare ($sql);
    $stm->execute (array ($pos, $lid, $tid));

    if ($stm->rowCount()==0)                // Do a check if done
        return array ('error'=>'can not add it, for some reason');
    else                                    // ok good, give me db result
    {
        $res = array();
        $res['success'] = 'OK';
        return $res;
    }
}

/**
 *  [getListById gets All tracklist that user have from db]
 *  @method getAllMyList
 *  @param  [string]                        [$lId = tracklistID]
 *  @param  [string]                        [uId = User ID]
 *  @return [array]                         [List of tracklist data for this user]
 */
function getListById($lId, $uId)
{
    global $db;
    $sql = 'SELECT tlid, name, description, addtime, private FROM tracklist WHERE owner=? AND tlid=?';
    
    $stm = $db->prepare ($sql);
    $stm->execute (array ($uId, $lId));
    $userTracklists = $stm->fetchAll(PDO::FETCH_ASSOC);
    
    return $userTracklists;
}

/**
 *  [getListById gets All tracklist that user have from db]
 *  @method getAllMyList
 *  @param  [string]  $id          [User id]
 *  @return [array]                [description]
 */
function getAllMyTracksInList($lId)
{
    global $db;
    $res = array();
    $sql = 'SELECT trackposition.position, track.id, track.name From trackposition INNER JOIN track ON track.id = trackposition.trackid WHERE trackposition.tracklistid=?';
    
    $stm = $db->prepare ($sql);
    $stm->execute (array ($lId));
    $res = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    return $res;         
}

/**
 *  [getAllVideosNotInList gets All tracks that user own and dpn't in this List from db]
 *  @method getAllVideosNotInList
 *  @param  [string]                        [$newId = tracklistID]
 *  @param  [string]                        [Sender = User ID]
 *  @return [array]                         [List with tracks informastin]
 */
function getAllVideosNotInList($newId, $sender)
{
    global $db;
    $res = array();
    $sql = 'SELECT track.id, track.name, trackposition.tracklistid, track.owner FROM track LEFT OUTER JOIN trackposition ON track.id = trackposition.trackid WHERE (track.owner = ?) AND (trackposition.tracklistid IS NULL OR trackposition.tracklistid != ?)';
    
    $stm = $db->prepare ($sql);
    $stm->execute (array ($sender, $newId));
    $res = $stm->fetchAll(PDO::FETCH_ASSOC);
    
    return $res;         
}

/**
 *  [updateList Update tracklist information in db]
 *  @method updateList
 *  @param  [string]                        [$iD = tracklistID]
 *  @param  [string]                        [$newN = tracklist name]
 *  @param  [string]                        [$newS = tracklist description]
 *  @param  [string]                        [$newP = tracklist private]
 *  @return [Array]                         [Success message or error]
 */
function updateList($iD, $newN, $newS, $newP)
{
    global $db;
    $res = array();
    $sql = 'UPDATE tracklist SET name = ?, description = ?, private = ? Where tlid = ?';
    
    $stm = $db->prepare ($sql);
    $stm->execute (array ($newN, $newS, $newP, $iD));
    
    if ($stm->rowCount()==0)                // Do a check if done
        return array ('error'=>'can not add it, for some reason');
    else                                    // ok good, give me db result
    {
        $res = array();
        $res['success'] = 'OK';
        return $res;
    }       
}

/**
 *  [removeAlltracksFromList will remove all information about the list from db to set it 
 *   up again]
 *  @method removeAlltracksFromList
 *  @param  [string]                        [$iD = tracklistID]
 *  @return [Array]                         [Success message or error]
 */
function removeAlltracksFromList($iD)
{
    global $db;
    $res = array();
    $sql = 'DELETE FROM trackposition WHERE trackposition.tracklistid = ?';
    
    $stm = $db->prepare ($sql);
    $stm->execute (array ($iD));
    
    if ($stm->rowCount()==0)                // Do a check if done
        return array ('error'=>'can not add it, for some reason');
    else                                    // ok good, give me db result
    {
        $res = array();
        $res['success'] = 'OK';
        return $res;
    }   
}



/**
 *  [removeThisList will remove all information about the list from db to set it 
 *   up again]
 *  @method removeAlltracksFromList
 *  @param  [string]                        [$iD = tracklistID]
 */
function removeThisList($iD)
{
    global $db;
    $res = array();
    $sql = 'DELETE FROM tracklist WHERE tlid = ?';
    
    $stm = $db->prepare ($sql);
    $stm->execute (array ($iD));
}




