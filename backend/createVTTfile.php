<?php
/**
 *  Gets JSON object and transform it into .vtt file, saves the last version as
 *  videoName.mp4/videoname.vtt, if exists from before copy the old one as
 *  videoName.mp4/videoname[X].vtt where [x] increments and is a backup
 *  possible to limit number of backup files and rotate them
 */
require_once 'accessControlAllowOrigin.php';
require_once 'userModel.php';
require_once 'subsModel.php';
require_once("videoModel.php");
session_start();

$result = array();


if (isset($_SESSION['userId'])) {
  $userClearance = getClearance($_SESSION['userId']);
  if ($userClearance < 3) {
    $result['success'] = 'Session reestablished,  trying to save a VTT file...';
// retrieving send JSON data
    $request_body = file_get_contents('php://input');
    // decode and force to be an associative array (.. , true)
    $receivedData = json_decode($request_body, true);

    $cueArray = $receivedData['cues'];  // text lines

    $vttBaseFile = $receivedData['textFilePath'];//"" +  $videoPath + "/" + $videoName + ".vtt";
    $result['filePath'] = $vttBaseFile;

    if (isset($receivedData['videoId'])) {
      $result['vidId'] = $receivedData['videoId'];
      $videoId = $receivedData['videoId'];
    } else {
      $videoId = "875f0bf2-2e9e-11e7-ba7a-d8cb8a805ab4";  /// test default
      $result['vidId'] = "not send";
    }
        // add information about the file to db
    $videoObject = new Video($db, $videoId);
    $videoObject->addSubtextLocal($vttBaseFile);


    $vttFile = fopen($vttBaseFile, "w");
    fwrite($vttFile, "WEBVTT FILE\n");

    foreach ($cueArray as $cue) {
      fwrite($vttFile, "\n");     // empty line
      if ($cue['id']) {
        fwrite($vttFile, $cue['id']."\n");  // id if exists
      }
      fwrite($vttFile, $cue['startTime']." --> ".$cue['endTime']."\n");
      fwrite($vttFile, $cue['text']."\n");
    }

  } else {
  	// not admin or teacher
  	$result['error'] = 'Student users cannot save VTT file';
  }
} else {
	// feil PHPSSID
	$result['error'] = 'Session not reestablished. Only inlogged users can save VTT file';
}

echo json_encode($result);
?>
