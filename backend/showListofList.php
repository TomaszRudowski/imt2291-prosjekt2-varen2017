<?php
/**
 *  [Get list of tracklist from backend to frontend, All list that a user have will be returned]
 */
require_once 'accessControlAllowOrigin.php';
require_once "listModel.php";

$tracklists = getAllMyList($_SESSION['userId']);

echo json_encode($tracklists);
?>
