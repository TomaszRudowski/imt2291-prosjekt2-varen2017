<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Setup</title>
    <script
  src="https://code.jquery.com/jquery-2.2.4.js"
  integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </head>
  <body>
    <style media="screen">
      html, body {
        background-color: #4444FF;
        color: white;
      }
      .glyphicon {
        color: #4F4;
      }

      .glyphicon-remove {
        color: #F44;
      }
      .src {
        height: 300px;
        overflow-y: scroll;
      }
    </style>

    <?php
    error_reporting(0);
    require_once("dbconn.php");
    require_once("function.php");
    $dbs = include("dbconfig.php");
    $okNess = 0;

     ?>
    <div class="container">
      <div class="Skriverettighet">
        <?php // Sjekker skriverettighet
        if(!is_writable(".")) {
          echo "
        <h1>Skriverettigheter</h1>
        <p>Du må ha skriverettigheter i denne mappen</p>
        <samp>chmod 775 rootdirectory</samp>";
        $okNess += 1;
      } else {
        echo '<h1>Skriverettigheter <span class="glyphicon glyphicon-ok"></span></h1>';
      }
        ?>
      </div>

      <div class="Databaseoppkobling">
        <?php
          if (isset($dbs['host'])){   // FIl er konfigurert
            $database = openConnection($dbs['host'], $dbs['user'], $dbs['pass'], $dbs['database']);
            echo '<h1>Databaseoppkobling <span class="glyphicon glyphicon-ok"></span></h1>';
            $okNess += 1;
          } else if(isset($_POST['sadr'])) {  // Verdier for database er satt
            $database = openConnection($_POST['sadr'], $_POST['uname'], $_POST['spass']);
            if(empty($database)) {  // Dersom verdier er feil
              echo '<h1>Koble til database</h1>
              <span class="label label-warning">Feil innloggings verdier</span>
              <form  method="post">
                <div class="form-group">
                  <label for="lh">Server Adress</label>
                  <input id="lh" class="form-control" type="text" name="sadr" value="localhost">
                </div>
                <div class="form-group">
                  <label for="un">Username</label>
                  <input id="un" class="form-control" type="text" name="uname" value="root">
                </div>

                <div class="form-group">
                  <label for="ps">password</label>
                  <input id="ps" class="form-control" type="text" name="spass" value="">
                  <input type="hidden" name="dbconn" value="">
                </div>
                <button type="submit" class="form-control btn btn-primary">Submit</button>
              </form>';
            } else {  // Riktige verdier
              echo '<h1>Databaseoppkobling <span class="glyphicon glyphicon-ok"></span></h1>';
            }

          } else {  // Hverken brukerinput eller konfigfil
            echo '
            <h1>Koble til database</h1>
            <form  method="post">
              <div class="form-group">
                <label for="lh">Server Adress</label>
                <input id="lh" class="form-control" type="text" name="sadr" value="localhost">
              </div>
              <div class="form-group">
                <label for="un">Username</label>
                <input id="un" class="form-control" type="text" name="uname" value="root">
              </div>

              <div class="form-group">
                <label for="ps">password</label>
                <input id="ps" class="form-control" type="text" name="spass" value="">
                <input type="hidden" name="dbconn" value="">
              </div>
              <button type="submit" class="form-control btn btn-primary">Submit</button>
            </form>
          </div>';
          }

         ?>


      <div class="OpprettDatabase">
        <?php
         if (isset($dbs['database'])) {
           echo '<h1>Database oppsatt <span class="glyphicon glyphicon-ok"></span></h1>';
           $okNess += 1;
         } else if(isset($_POST['dbname']) && isset($database)) {
           echo "koden ble kjørt";
           $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           $query = "CREATE DATABASE IF NOT EXISTS ".$_POST['dbname'];
           $res = $database->exec($query);

           $database = openConnection($_POST['sadr'], $_POST['uname'], $_POST['spass'], $_POST['dbname']);
           $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           $res = $database->exec(file_get_contents('database.sql'));
         } else if (isset($_POST['sadr'])) {
           echo '  <h1>Sett opp database</h1>
             <form method="post">
               <div class="form-group">
                 <label for="dbname">Databasenavn</label>
                 <input class="form-control" id="dbname" type="text" name="dbname" value="">
                 <input class="form-control" id="dbname" type="hidden" name="sadr" value="'.$_POST['sadr'].'">
                 <input class="form-control" id="dbname" type="hidden" name="uname" value="'.$_POST['uname'].'">
                 <input class="form-control" id="dbname" type="hidden" name="spass" value="'.$_POST['spass'].'">
               </div>
               <button type="submit" class="form-control btn btn-primary">Submit</button>
             </form>';
         } else {
           echo '<h1>Database oppsatt <span class="glyphicon glyphicon-remove"></span></h1>';
           echo "<p>Du må koble deg til databasen først</p>";
         }
         ?>
      </div>
      <div class="furtherSetup">

      </div>
      <?php
      if(okNess == 2){
        echo "<h1>Attemting setup of urlConfig.js</h1>";

        $myfile = fopen('../src/urlConfig.js', "w");
        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $actual_link = dirname($actual_link);

        $pts = explode('/', $actual_link);
        array_pop($pts);
        $actual_link = implode('/', $pts);

        fwrite($myfile,'var urlRoot="'. $actual_link.'";');
        fclose($myfile);
        chdir("..");
        $v = shell_exec("bower update");
        echo "<div class='src'><samp>$v</samp></div>";
      }
       ?>


    </div>
  </body>
</html>
