

<?php
// Funksjonen er inspirert av w3schools eksempel på filopplastning
// https://www.w3schools.com/php/php_file_upload.asp
require_once("videoModel.php");
error_reporting(E_ERROR);

require_once("db.php");
//session_start();

require_once("accessControlAllowOrigin.php");

session_start();

$result = array();  // Array for getting messages to frontend

$model = new Video($db);  // Creates model object
$ok=1;  // Legal until proven guilty
$target = "videos/";
$target = $target . hash("md5", basename($_FILES["file"]["name"] . time()));  // generates unique folder name

$targetSize = $_FILES['file']['size'];
$finfo = finfo_open(FILEINFO_MIME_TYPE);
$uploaded_type = $_FILES['file']['type'];
finfo_close($finfo);
if(!mkdir($target, 0777, true)) {   // Makes forlder structure
  chmod($target, 0777);             // Changes permissoins of folder
}
$target = $target."/".$_FILES['file']['name']; // videos/videoname.vid/videoname.vid



 //This is our size condition
if ($targetSize > 150000000) { // Over størrelse
  $result['danger']['Filesize'] = "File is to large ";
  $ok=0;
}
$result['info']['thefiletype'] = $uploaded_type;
 // Kunn mp4 og vtt filer kan lastes opp. Alle andre kan ikke lastes opp
if ($uploaded_type != 'text/vtt' && $uploaded_type != 'video/mp4') {
  $ok = 0;
  $result['danger']['Filetype'] = "Wrong filetype $uploaded_type";
}

if ($ok==0)  // Opplastning ikke mulig på grunn av størrelse eller type
{

} else {  // Filen kan lastes opp
  if(move_uploaded_file($_FILES['file']['tmp_name'], $target)) { // Om filen kan flyttes til en mappe
    if($model->add($target, $_POST['viddesc'], $_POST['vidname']))     // Legg til filen i databasen
      $result['success']['upload'] = "upload successfull";
    else
      $result['warning']['database'] = "Could not add to database";



  } else {
    $result['warning']['servererror'] = "Could not move file. Need write permission";
    //unset($_FILES);
  }
}
echo json_encode($result);
?>
