<?php
// local setup ver.1.0
$dbs = include("dbconfig.php");



try {
 $db = new PDO("mysql:host={$dbs['host']};dbname={$dbs['database']}", $dbs['user'], $dbs['pass']);
} catch (PDOException $e) {
	echo $e;
	die ('Unable to connect to database, please try again later');
}
?>
