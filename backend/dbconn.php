<?php
require_once 'function.php';

function openConnection($server, $username, $password, $database=NULL) {
  if (empty($server) || empty($username)) {
      throw new InvalidArgumentException('Host, database name and username are required.');
  }
  if(isset($database))
    $dsn = "mysql:host=".$server.";dbname=".$database;
  else
    $dsn = "mysql:host=".$server.";";

  try {
      $db = new PDO ($dsn, $username, $password);
      if(isset($database)) {
        configWriter(array('host' => "$server", 'pass' => "$password",
        'user' => "$username", 'database' => "$database"));
      }


      return $db;
  } catch (PDOException $e) {
    //echo $e;
  }
}

?>
