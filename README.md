# project_2
Tomasz Rudowski, Martin Klingenberg og Ahmed S. M. Madhun
Project 2, IMT 2291

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your application locally.

## Clone repository

Create a directory in your htdocs directory, so we can use PHP server side and local path

## Update your local .gitignore !

bower_components/
test/
src/urlConfig.js
backend/db.php

## Viewing Your Application

```
$ polymer serve
```

## Building Your Application

We are not using this.

```
$ polymer build
```

This will create a `build/` folder with `bundled/` and `unbundled/` sub-folders
containing a bundled (Vulcanized) and unbundled builds, both run through HTML,
CSS, and JS optimizers.

You can serve the built versions by giving `polymer serve` a folder to serve
from:

```
$ polymer serve build/bundled
```

## Running Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.