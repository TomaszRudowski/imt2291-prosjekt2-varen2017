/**
 *  [Help functions to clear user data from forms and component properties]
 *
 */

/**
 *  [clearSignInForm resets form inputs]
 *  @method clearSignInForm
 *  @return {[type]}        [description]
 */
function clearSignInForm() {
  document.getElementById("signInEmail").value = "";
  document.getElementById("signInPassword").value = "";
  document.getElementById("signInCheckbox").checked = false;
}

/**
 *  [clearSignUpForm resets form inputs]
 *  @method clearSignUpForm
 *  @return {[type]}        [description]
 */
function clearSignUpForm() {
  document.getElementById("inputEmail").value = "";
  document.getElementById("inputPassword").value = "";
  document.getElementById("inputGivenname").value = "";
  document.getElementById("inputSurename").value = "";
  document.getElementById("inputClearance").value = "";
}

/**
 *  [clearUserNames resets user atribute in during logout/ user change]
 *  @method clearUserNames
 *  @return {[type]}       [description]
 */
function clearUserNames() {
  document.getElementById("adminControlPanel").user = "";
  document.getElementById("teacherControlPanel").user = "";
  document.getElementById("studentControlPanel").user = "";
}

/**
 *  [clearAddTracklistForm resets form inputs]
 *  @method clearAddTracklistForm
 *  @return {[type]}        [description]
 */
function clearAddTracklistForm() {
  document.getElementById("listTitle").value = "";
  document.getElementById("description").value = "";
    
  var num = document.getElementById('count');
  num.innerHTML = value = "0";
}

/**
 *  [clearAddTracklistForm resets form inputs]
 *  @method clearAddTracklistForm
 *  @return {[type]}        [description]
 */
function clearEditTracklistForm()
{
    var hide = document.getElementById('showListOfMyList').style.display = 'block';
    /*document.getElementById("listTitle").value = "";
  document.getElementById("description").value = "";
    
  var num = document.getElementById('count');
  num.innerHTML = value = "0";*/
}