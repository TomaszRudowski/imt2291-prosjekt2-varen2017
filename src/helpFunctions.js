/**
 *  [Help functions for now only to operate with cookies, but here is place for more "global functions" ]
 *
 */


/**
 *  [getCookie from bootstrap example]
 *  @method getCookie
 *  @param  {[type]}  cname [description]
 *  @return {[type]}        [description]
 */
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/**
 *  [setCookie from bootstrap example]
 *  @method setCookie
 *  @param  {[string]}  cname  [description]
 *  @param  {[string]}  cvalue [description]
 *  @param  {[int]}  exdays [expire time in days]
 */
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


function setAlert(emph, message, type) {

  var element = document.createElement("div");
  element.className= "alert alert-"+type+" alert-dismissable";
  var button = document.createElement("a");
  button.appendChild(document.createTextNode("x")); button.className = "close";
  button.setAttribute("data-dismiss", "alert");
  emphText = document.createElement("strong");
  emphText.appendChild(document.createTextNode(emph+": "));
  element.appendChild(button);
  element.appendChild(emphText);
  element.appendChild(document.createTextNode(message));


  document.getElementById("alertbox").appendChild(element);
}

function setAlertByObject(oja) {
  $('#alertbox').empty();
  for (var variable in oja) {
    if (oja.hasOwnProperty(variable)) {
      for (var key in oja[variable]) {
        if (oja[variable].hasOwnProperty(key)) {
          setAlert(key, oja[variable][key], variable);
        }
      }
    }
  }
}

function closeVideo() {
  document.getElementById("playerModule").resetVideoDiv();
  console.log("closing");
}
